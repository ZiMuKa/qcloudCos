package com.example.demo.controller;

import com.example.demo.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 *  文件相关接口
 */
@RestController
@RequestMapping("/file")
@Slf4j
public class FileController {

    @Resource
    private FileService fileService;

    @PostMapping("/fileUpload")
    @ResponseBody
    public String fileUpload(HttpServletRequest request,
                             @RequestParam(value = "file", required = false) MultipartFile multipartFile) throws Exception {

        String examId = request.getParameter("examId");
        String alias = request.getParameter("alias");

        if (StringUtils.isEmpty(alias)) {
            return "参数缺失";
        }
        // 文件夹名称（自定义存储文件夹，根据需要选择性添加）
        String folderName = "backGround-image";
        return fileService.fileUpload(folderName, multipartFile, alias);

    }

    @DeleteMapping("/fileDel")
    @ResponseBody
    public String fileDel(@RequestParam(value = "delFileUrl", required = true) String delFileUrl) {

        if (StringUtils.isEmpty(delFileUrl)) {
            return "参数缺失";
        }

        fileService.fileDel(delFileUrl);
        return "SUCCESS";
    }
}
