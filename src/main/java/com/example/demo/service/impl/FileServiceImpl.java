package com.example.demo.service.impl;

import com.example.demo.config.CosConfig;
import com.example.demo.service.FileService;
import com.example.demo.util.CosFileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.InputStream;

/**
 * FileService 实现类
 */
@Service
public class FileServiceImpl implements FileService {

    @Resource
    private CosFileUtil cosFileUtil;

    @Autowired
    private CosConfig cosConfig;

    /**
     * 接口描述：图片上传
     */
    @Override
    public String fileUpload(String folderName, MultipartFile multipartFile, String alias) throws Exception{

        if (multipartFile.getSize() < 0 || multipartFile.getSize() > 10485760) {
            return "上传图片不能为空或超过10MB";
        }

        InputStream inputStream = multipartFile.getInputStream();
        File file = new File(multipartFile.getOriginalFilename());
        // 转换后会生成临时文件
        cosFileUtil.inputStreamToFile(inputStream, file);

        String returnUrl = cosFileUtil.picCOS(folderName, file, alias);

        // 删除临时文件
        File del = new File(file.toURI());
        del.delete();

        return returnUrl;
    }

    /**
     * 接口描述：删除图片
     * 参数delFileUrl为节点域名（即腾讯云静态资源访问域名）+存储桶名称+文件路径，如：
     * https://test-mini-image-**********.cos-website.ap-beijing.myqcloud.com/test-mini-image-**********（存储桶名称）/001.png
     */
    @Override
    public void fileDel(String delFileUrl) {

        String[] index = delFileUrl.split("com/");
        if (index.length < 2) {
            System.out.println("参数异常");
        }
        String domainName = index[0] + "com";
        // 校验节点域名是否有效
        if (domainName.equals(cosConfig.getDomainName())) {
            cosFileUtil.delFileCOS(index[1]);
        }
    }
}
