package com.example.demo.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * FileService
 */
public interface FileService {

    /**
     * 接口描述：图片上传
     */
    String fileUpload(String folderName, MultipartFile multipartFile, String alias) throws Exception;

    /**
     * 接口描述：删除图片
     */
    void fileDel(String delFileUrl);
}
